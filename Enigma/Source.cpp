#include <iostream>

typedef struct Rotor{
	int* numbers_R; //to reflector
	int* numbers_L; //from reflector
	int* turnoversNumbers;
	int position;
	int numbersCount;
	int turnoversCount;
};

typedef struct Reflector{
	int* numbers;
	int numbersCount;
};
typedef struct Enigma {
	int* taskNumbers;//input
	int numbersLenght;//input lentgh
	Rotor** rotors;
	Rotor** taskRotors;
	Reflector** reflectors;
	Reflector* taskReflector;
	int tMoved;
	int sMoved;
	int fMoved;//rotator state
	int rotorsCount;
	int taskRotorsCount;
	int reflectorsCount;
	int numbersCount;//alphabet size

};

void printEnigma(const Enigma* enigma);
Reflector* defineReflector(int alphabetsize);
Rotor* defineRotor(int alphabetsize);
Enigma* getdefinitions();
void copyRotor(Rotor* dest, Rotor* src);
void copyReflector(Reflector* dest, Reflector* src);
void getNumber(Enigma* enigma);
int getTaskRotorsCount(Enigma* enigma);
void prepareTask(Enigma* enigma);
void clearTask(Enigma* enigma);
void clearEnigma(Enigma* enigma);


int mod(int a, int b) {
	return (a % b + b) % b;
}


void encryptRotor(Rotor* rotor, int& input,bool toReflector) {
	input=mod(input + rotor->position,rotor->numbersCount);

	if(toReflector)
		input = rotor->numbers_R[input];
	else
		input = rotor->numbers_L[input];

	input = mod(input+rotor->numbersCount- rotor->position, rotor->numbersCount);
}

void encryptReflector(Reflector* reflector, int &input) {

	input=reflector->numbers[input];
}

void setRotorPosition(Rotor* rotor, int number) {
	rotor->position = mod((number+rotor->numbersCount-1),rotor->numbersCount);	//shift number
}

int isTurnoverPosition(Rotor* rotor) {
	for (int i = 0; i < rotor->turnoversCount; i++)
		if (rotor->turnoversNumbers[i]==(mod((rotor->position + 1), rotor->numbersCount))) {
			return true;
		}
	return false;	
}

void moveRotor(Enigma* enigma,int rotorIndex) {	
	Rotor* rotor = enigma->taskRotors[rotorIndex];
	rotor->position = mod(rotor->position + 1, enigma->numbersCount);
}


void checkStateOfRotors(Enigma* enigma)
{
	switch (getTaskRotorsCount(enigma))
	{
		case (3):
			if (enigma->sMoved && isTurnoverPosition(enigma->taskRotors[1]))
			{
				moveRotor(enigma, 2);
				enigma->tMoved = true;
			}
		case 2:
			if ((enigma->fMoved && isTurnoverPosition(enigma->taskRotors[0])||enigma->tMoved)) {
				enigma->tMoved = false;
				moveRotor(enigma, 1);//second rotor, moves only at turnover position of first rotor
				enigma->sMoved = true;
			}
		case 1:
			moveRotor(enigma, 0);//first rotor, moves every time
			enigma->fMoved = true;
			break;
	}	
}
void doEncryption(Enigma* enigma,int i) {
	for (int j = 0; j < enigma->taskRotorsCount; j++)
	{
		encryptRotor(enigma->taskRotors[j], enigma->taskNumbers[i],true);
	}

	encryptReflector(enigma->taskReflector, enigma->taskNumbers[i]);

	for (int j = enigma->taskRotorsCount-1; j>=0; j--)
	{
		encryptRotor(enigma->taskRotors[j], enigma->taskNumbers[i],false);
	}
}

void doTask(Enigma* enigma) {		
	for (int i = 0; i < enigma->numbersLenght; i++)
	{
		
		checkStateOfRotors(enigma);
		doEncryption(enigma,i);

		printf("%i ", enigma->taskNumbers[i]+1);
	
	}
	printf("\n");
}





int main() {
	Enigma* enigma = getdefinitions();
	//printEnigma(enigma);
	int taskCount;
	scanf("%i", &taskCount);
	for (int i = 0; i < taskCount; i++)
	{
		prepareTask(enigma);
		doTask(enigma);
		clearTask(enigma);

	}
	clearEnigma(enigma);	
	return 0;
}


void printEnigma(const Enigma* enigma) {
	system("cls");
	printf("Numbers count: %i\n", enigma->numbersCount);
	printf("Rotors count: %i\n Rotors:\n", enigma->rotorsCount);
	for (int i = 0; i < enigma->rotorsCount; i++) {
		printf("Rotor_%i\n Numbers count:%i Numbers:\n", i, enigma->rotors[i]->numbersCount);
		for (int j = 0; j < enigma->numbersCount; j++)
		{
			printf("%i\t", enigma->rotors[i]->numbers_R[j]);
		}
		printf("\n\n");
		printf("Rotor_%i TurnOversCount:%i\n Numbers:\n", i, enigma->rotors[i]->turnoversCount);
		for (int j = 0; j < enigma->rotors[i]->turnoversCount; j++)
		{
			printf("%i\t", enigma->rotors[i]->turnoversNumbers[j]);
		}
		printf("\n\n");
	}
	printf("\n");
	printf("Reflectors count: %i\n Reflectors:\n", enigma->reflectorsCount);
	for (int i = 0; i < enigma->reflectorsCount; i++) {
		printf("Reflector_%i\n Numbers:\n", i);
		for (int j = 0; j < enigma->numbersCount; j++)
		{
			printf("%i\t", enigma->reflectors[i]->numbers[j]);
		}
		printf("\n");
	}

}
Reflector* defineReflector(int alphabetsize) {
	Reflector* reflector = (Reflector*)malloc(sizeof(Reflector));
	reflector->numbersCount = alphabetsize;
	reflector->numbers = (int*)malloc(sizeof(int) * alphabetsize);

	for (int i = 0; i < reflector->numbersCount; i++)
	{
		scanf("%i", &reflector->numbers[i]);
		reflector->numbers[i]--;
	}

	return reflector;
}
Rotor* defineRotor(int alphabetsize) {
	Rotor* rotor = (Rotor*)malloc(sizeof(Rotor));
	rotor->numbersCount = alphabetsize;
	rotor->numbers_R = (int*)malloc(sizeof(int) * alphabetsize);
	rotor->numbers_L = (int*)malloc(sizeof(int) * alphabetsize);

	for (int i = 0; i < rotor->numbersCount; i++)//init numbers to reflector
	{
		scanf("%i", &rotor->numbers_R[i]);
		rotor->numbers_R[i]--;
	}
	for (int i = 0; i < rotor->numbersCount; i++)//init numbers from reflector
	{
		rotor->numbers_L[rotor->numbers_R[i]] = i;
	}

	scanf("%i", &rotor->turnoversCount);

	rotor->turnoversNumbers= rotor->turnoversCount>0?(int*)malloc(sizeof(int) * rotor->turnoversCount):NULL;

	for (int i = 0; i < rotor->turnoversCount; i++)
	{
		scanf("%i", &rotor->turnoversNumbers[i]);
		rotor->turnoversNumbers[i]--;
	}	

	return rotor;
}
Enigma* getdefinitions() {
	Enigma* enigma = (Enigma*)malloc(sizeof(Enigma));
	scanf("%i", &enigma->numbersCount);
	scanf("%i", &enigma->rotorsCount);



	enigma->rotors = (Rotor**)malloc(sizeof(Rotor*) * enigma->rotorsCount);
	for (int i = 0; i < enigma->rotorsCount; i++)
	{
		enigma->rotors[i] = defineRotor(enigma->numbersCount);
	}
	scanf("%i", &enigma->reflectorsCount);
	enigma->reflectors = (Reflector**)malloc(sizeof(Reflector*) * enigma->reflectorsCount);
	for (int i = 0; i < enigma->reflectorsCount; i++)
	{
		enigma->reflectors[i] = defineReflector(enigma->numbersCount);
	}

	return enigma;
}
void copyRotor(Rotor* dest, Rotor* src) {
	dest->numbersCount = src->numbersCount;
	dest->numbers_R = (int*)malloc(sizeof(int) * src->numbersCount);
	dest->numbers_L = (int*)malloc(sizeof(int) * src->numbersCount);
	for (int i = 0; i < src->numbersCount; i++)
	{
		dest->numbers_R[i] = src->numbers_R[i];
		dest->numbers_L[i] = src->numbers_L[i];
	}
	dest->turnoversCount = src->turnoversCount;
	dest->turnoversNumbers = src->turnoversCount > 0 ? (int*)malloc(sizeof(int) * src->turnoversCount) : NULL;
	for (int i = 0; i < src->turnoversCount; i++)
	{
		dest->turnoversNumbers[i] = src->turnoversNumbers[i];
	}
}
void copyReflector(Reflector* dest, Reflector* src) {
	dest->numbersCount = src->numbersCount;
	dest->numbers = (int*)malloc(sizeof(int) * src->numbersCount);
	for (int i = 0; i < src->numbersCount; i++)
	{
		dest->numbers[i] = src->numbers[i];
	}
}
void getNumber(Enigma* enigma) {

	unsigned int chunk_size = 16;
	unsigned int current_size = chunk_size;
	enigma->taskNumbers = (int*)malloc(sizeof(int) * chunk_size);
	if (enigma->taskNumbers != NULL)
	{
		unsigned int i = 0;
		int num;

		while ((scanf("%i", &num) == 1) && num != 0)
		{
			enigma->taskNumbers[i++] = num-1;

			if (i == current_size)
			{
				current_size = i + chunk_size;
				enigma->taskNumbers = (int*)realloc(enigma->taskNumbers, sizeof(int) * current_size);
			}
		}

		enigma->numbersLenght = i;
	}
}
int getTaskRotorsCount(Enigma* enigma)
{
	return enigma->taskRotorsCount >= 3 ? 3 : enigma->taskRotorsCount;
}
void prepareTask(Enigma* enigma) {
	int rotorsCount;
	int reflectorIndex;
	scanf("%i", &rotorsCount);
	enigma->taskRotorsCount = rotorsCount;
	enigma->taskRotors = (Rotor**)malloc(sizeof(Rotor*) * rotorsCount);
	enigma->fMoved = false;
	enigma->sMoved= false;
	enigma->tMoved= false;
	for (int i = 0; i < rotorsCount; i++) {
		int index;
		int position;
		scanf("%i", &index);
		scanf("%i", &position);

		enigma->taskRotors[i] = (Rotor*)malloc(sizeof(Rotor));
		copyRotor(enigma->taskRotors[i], enigma->rotors[index]);
	
		setRotorPosition(enigma->taskRotors[i], position);
		
	}
	scanf("%i", &reflectorIndex);
	enigma->taskReflector = (Reflector*)malloc(sizeof(Reflector));
	copyReflector(enigma->taskReflector, enigma->reflectors[reflectorIndex]);
	getNumber(enigma);
	/*system("cls");
	for (int i = 0; i < enigma->numbersLenght; i++)
	{
		printf("%i\t", enigma->taskNumbers[i]);
	}*/
}
void clearTask(Enigma* enigma) {
	for (int i = 0; i < enigma->taskRotorsCount; i++)
	{
		free(enigma->taskRotors[i]->numbers_L);
		free(enigma->taskRotors[i]->numbers_R);
		free(enigma->taskRotors[i]->turnoversNumbers);
	}
	free(enigma->taskNumbers);
	free(enigma->taskRotors);
	free(enigma->taskReflector);
}
void clearEnigma(Enigma* enigma) {
	for (int i = 0; i < enigma->rotorsCount; i++)
	{
		free(enigma->rotors[i]->numbers_R);
		free(enigma->rotors[i]->numbers_L);
		free(enigma->rotors[i]->turnoversNumbers);
		free(enigma->rotors[i]);
	}
	for (int i = 0; i < enigma->reflectorsCount; i++)
	{
		free(enigma->reflectors[i]->numbers);
		free(enigma->reflectors[i]);
	}
	free(enigma->rotors);
	free(enigma->reflectors);
	free(enigma);
}